#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Batch Tax Timeline',
    'name_de_DE': 'Buchhaltung Stapelbuchung Steuer Gültigkeitsdauer',
    'version': '2.2.0',
    'author': 'virtual-things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Timeline for Tax on Batch Entries
    - Provides the functionality of account_timeline for
     account_batch_tax.
''',
    'description_de_DE': '''Gültigkeitsdauer für Steuern in der Stapelbuchung
    - Stellt die Merkamale der Gültigkeitsdauer-Module für die Steuern in der
    Stapelbuchung bereit.
''',
    'depends': [
            'account_batch_tax',
            'account_batch_timeline',
    ],
    'xml': [
    ],
    'translation': [
    ],
}
