#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL
from trytond.transaction import Transaction


class Line(ModelSQL, ModelView):
    _name = 'account.batch.line'

    def _get_move_lines(self, batch_line_vals):
        with Transaction().set_context(effective_date=batch_line_vals['date']):
            res = super(Line, self)._get_move_lines(batch_line_vals)
        return res

Line()


